# Field Compare Module

This module helps site builders to configure their fields consistently. It makes
is easy to compare field configuration across content types. It provides
overviews that contains all fields of all entity bundles (e.g. node types) that
are available in your site. You can compare field configuration side by side
instead of clicking through numerous pages.

This module should not be used on live sites because it is intended for site
building only.
