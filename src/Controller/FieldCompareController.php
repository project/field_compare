<?php

namespace Drupal\field_compare\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\field_compare\FieldCompareOverviewInterface;
use Drupal\field_compare\OverviewSettingsInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Page and title callbacks for Field Compare routes.
 */
class FieldCompareController extends ControllerBase {

  /**
   * The field compare manager.
   *
   * @var \Drupal\field_compare\FieldCompareOverviewInterface
   */
  protected FieldCompareOverviewInterface $fieldCompareManager;

  /**
   * The field compare overview settings service.
   *
   * @var \Drupal\field_compare\OverviewSettingsInterface
   */
  protected OverviewSettingsInterface $overviewSettings;

  /**
   * Constructor.
   *
   * @param \Drupal\field_compare\FieldCompareOverviewInterface $fieldCompareManager
   *   The field compare manager.
   * @param \Drupal\field_compare\OverviewSettingsInterface $overviewSettings
   *   The field compare overview settings service.
   */
  public function __construct(FieldCompareOverviewInterface $fieldCompareManager, OverviewSettingsInterface $overviewSettings) {
    $this->fieldCompareManager = $fieldCompareManager;
    $this->overviewSettings = $overviewSettings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): FieldCompareController {
    return new static(
      $container->get('field_compare.overview'),
      $container->get('field_compare.settings')
    );
  }

  /**
   * Build an index page of field config comparisons pages.
   *
   * @return array
   *   Drupal render array.
   */
  public function indexPage(): array {

    $items = [];
    foreach ($this->entityTypeManager()->getDefinitions() as $definition) {
      if ($definition instanceof ContentEntityType
        && is_subclass_of($definition->getOriginalClass(), 'Drupal\Core\Entity\FieldableEntityInterface')
      ) {
        if ($this->hasFieldConfigs($definition->id())) {
          $items[] = Link::createFromRoute($definition->getLabel(), 'field_compare.entity_overview', ['entity_type' => $definition->id()]);
        }
      }
    }

    return [
      '#theme' => 'item_list',
      '#items' => $items,
      '#cache' => ['max-age' => 0],
    ];
  }

  /**
   * Builds a field comparison overview per entity type.
   *
   * @param string $entity_type
   *   The entity type to use.
   *
   * @return array
   *   Drupal render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   If the entity type is not found.
   */
  public function entityTypeOverviewPage(string $entity_type): array {

    $definition = $this->entityTypeManager()->getDefinition($entity_type, FALSE);
    if (empty($definition)) {
      throw new NotFoundHttpException();
    }
    if (!is_a($definition->getOriginalClass(), 'Drupal\Core\Entity\FieldableEntityInterface', TRUE)) {
      throw new NotFoundHttpException();
    }

    $build['form'] = [
      '#type' => 'details',
      '#title' => $this->t('Display settings'),
      '#open' => FALSE,
      'settings' => $this->formBuilder()->getForm('\Drupal\field_compare\Form\OverviewSettingsForm'),
    ];
    ;
    $build['table'] = $this->fieldCompareManager->buildEntityTypeTable($definition->id());
    return $build;
  }

  /**
   * Title callback for field comparison overview page.
   *
   * @param string $entity_type
   *   The entity type to use.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title.
   */
  public function entityTypeOverviewTitle(string $entity_type): TranslatableMarkup {

    $definition = $this->entityTypeManager()->getDefinition($entity_type, FALSE);
    if (empty($definition)) {
      $this->t('Compare field configuration');
    }

    return $this->t('Compare fields of entity type @entity_type', ['@entity_type' => $definition->getLabel()]);
  }

  /**
   * Ajax callback to update the configuration form settings.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The ajax request object.
   */
  public function ajaxSettingsUpdate(Request $request) {

    $entityType = $request->request->get('entityType');
    $visible = [
      'field' => $request->request->get('visibleFieldSettings'),
      'storage' => $request->request->get('visibleStorageSettings'),
      'widget' => $request->request->get('visibleWidgetSettings'),
      'formatter' => $request->request->get('visibleFormatterSettings'),
    ];

    $settings = [
      'visible' => array_filter($visible),
      'hide_equal' => filter_var($request->request->get('hideEqual'), FILTER_VALIDATE_BOOLEAN),
      'form_mode' => $request->request->get('formMode'),
      'view_mode' => $request->request->get('viewMode'),
    ];
    $this->overviewSettings->setSettings($entityType, $settings);
  }

  /**
   * Checks if an entity type has fields configured.
   *
   * @param string $entityType
   *   The entity type to check.
   *
   * @return bool
   *   True if so.
   */
  protected function hasFieldConfigs(string $entityType): bool {
    $fieldConfigs = $this->entityTypeManager->getStorage('field_config')
      ->loadByProperties([
        'entity_type' => $entityType,
      ]);

    return !empty($fieldConfigs);
  }

}
