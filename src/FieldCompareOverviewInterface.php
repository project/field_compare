<?php

namespace Drupal\field_compare;

/**
 * The FieldCompareOverview service.
 */
interface FieldCompareOverviewInterface {

  /**
   * Group name of field settings.
   */
  const FIELD_SETTINGS_GROUP_NAME = 'field';

  /**
   * Group name of field storage settings.
   */
  const STORAGE_SETTINGS_GROUP_NAME = 'storage';

  /**
   * Group name of field widget settings.
   */
  const WIDGET_SETTINGS_GROUP_NAME = 'widget';

  /**
   * Group name of field formatter settings.
   */
  const FORMATTER_SETTINGS_GROUP_NAME = 'formatter';

  /**
   * Builds a comparison table of field configurations.
   *
   * @param string $entityType
   *   The entity type for which to build the table.
   * @param string $formMode
   *   The form mode to use.
   * @param string $viewMode
   *   The view mode to use.
   *
   * @return array
   *   Drupal render array.
   */
  public function buildEntityTypeTable(string $entityType, string $formMode = 'default', string $viewMode = 'default'): array;

}
