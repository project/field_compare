<?php

namespace Drupal\field_compare;

use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * A helper service for Field Compare overview settings.
 */
class OverviewSettings implements OverviewSettingsInterface {

  /**
   * The field_compare private tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected PrivateTempStore $privateTempStore;

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   *   .
   */
  protected Request $request;

  /**
   * Constructs a FieldCompareOverview object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $privateTempStoreFactory
   *   The private temporary storage factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(PrivateTempStoreFactory $privateTempStoreFactory, RequestStack $requestStack) {
    $this->privateTempStore = $privateTempStoreFactory->get('field_compare');
    $this->request = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings(string $entityType, bool $override = TRUE): array {
    $settings = $this->getDefaultSetting();
    $storedValues = $this->privateTempStore->get($entityType) ?: [];
    foreach ($storedValues as $key => $value) {
      $settings[$key] = $value;
    }

    // Override the stored values with values from the current URL.
    if ($override) {
      foreach ($this->getSettingsFromUrl() as $key => $value) {
        $settings[$key] = $value;
      }
    }

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function setSettings(string $entityType, array $settings) {
    $storedSettings = $this->getSettings($entityType, FALSE);
    foreach (array_intersect_key($settings, $this->getDefaultSetting()) as $key => $value) {
      $storedSettings[$key] = $value;
    }

    $this->privateTempStore->set($entityType, $storedSettings);
  }

  /**
   * Returns overview settings as provided by the URL query keys.
   *
   * @return array
   *   Associative array of overview settings. Keys by settings machine name.
   */
  protected function getSettingsFromUrl(): array {
    $settings = [];

    // Only those query keys are considered that are defined in
    // ::getDefaultSetting(). This prevents unknown keys from entering the flow.
    foreach (array_keys($this->getDefaultSetting()) as $key) {

      $value = $this->request->get($key);
      if (!is_null($value)) {
        $settings[$key] = $value;
      }
    }

    return $settings;
  }

  /**
   * Returns defaults of overview settings.
   *
   * @return array
   *   Associative array of overview settings. Keys by settings machine name.
   */
  protected function getDefaultSetting(): array {
    return [
      'visible' => [
        FieldCompareOverviewInterface::FIELD_SETTINGS_GROUP_NAME,
        FieldCompareOverviewInterface::STORAGE_SETTINGS_GROUP_NAME,
        FieldCompareOverviewInterface::WIDGET_SETTINGS_GROUP_NAME,
        FieldCompareOverviewInterface::FORMATTER_SETTINGS_GROUP_NAME,
      ],
      'hide_equal' => FALSE,
      'form_mode' => 'default',
      'view_mode' => 'default',
    ];
  }

}
