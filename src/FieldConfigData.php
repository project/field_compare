<?php

namespace Drupal\field_compare;

/**
 * Data object to store configuration data.
 */
class FieldConfigData implements FieldConfigDataInterface {

  /**
   * The configuration label.
   *
   * @var string
   */
  protected string $label = '';

  /**
   * The configuration machine name.
   *
   * Nested configuration names are the combined name of child and parent names
   * separated by dots.
   *
   * @var string
   */
  protected string $name = '';

  /**
   * The configuration data type.
   *
   * @var string
   */
  protected string $type = '';

  /**
   * The configuration value formatted as string.
   *
   * @var string
   */
  protected string $value = '';

  /**
   * The field label.
   *
   * @var FieldConfigDataInterface[]
   */
  protected array $children = [];

  /**
   * Hash to determine uniqueness.
   *
   * @var string
   */
  protected string $hash = '';

  /**
   * {@inheritdoc}
   */
  public function setLabel(string $label): FieldConfigData {
    $this->label = $label;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setName(string $name): FieldConfigData {
    $this->name = $name;
    $this->updateHash();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setType(string $type): FieldConfigData {
    $this->type = $type;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue(string $value): FieldConfigData {
    $this->value = $value;
    $this->updateHash();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setChildren(array $children): FieldConfigData {
    $this->children = $children;
    $this->updateHash();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(): string {
    return $this->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChildren(): array {
    return $this->children;
  }

  /**
   * {@inheritdoc}
   */
  public function getHash(): string {
    return $this->hash;
  }

  /**
   * Updates the value hash.
   */
  protected function updateHash() {
    $childHashes = [];
    foreach ($this->getChildren() as $child) {
      $childHashes[] = $child->getHash();
    }
    $this->hash = md5("{$this->name}:{$this->value}:" . implode(':', $childHashes));
  }

}
