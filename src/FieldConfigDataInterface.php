<?php

namespace Drupal\field_compare;

/**
 * Interface for configuration data object.
 */
interface FieldConfigDataInterface {

  /**
   * Set the data label.
   *
   * @param string $label
   *   The data label.
   *
   * @return FieldConfigData
   *   The updated config data object.
   */
  public function setLabel(string $label): FieldConfigData;

  /**
   * Set the configuration name (path).
   *
   * @param string $name
   *   The data name.
   *
   * @return FieldConfigData
   *   The updated config data object.
   */
  public function setName(string $name): FieldConfigData;

  /**
   * Set the data type.
   *
   * @param string $type
   *   The data type.
   *
   * @return FieldConfigData
   *   The updated config data object.
   */
  public function setType(string $type): FieldConfigData;

  /**
   * Set the data value formatted as string.
   *
   * @param string $value
   *   The data value.
   *
   * @return FieldConfigData
   *   The updated config data object.
   */
  public function setValue(string $value): FieldConfigData;

  /**
   * Set the data children.
   *
   * @param \Drupal\field_compare\FieldConfigDataInterface[] $children
   *   The child object(s).
   *
   * @return FieldConfigData
   *   The updated config data object.
   */
  public function setChildren(array $children): FieldConfigData;

  /**
   * Returns the data label.
   *
   * @return string
   *   The data label.
   */
  public function getLabel(): string;

  /**
   * Returns the data machine name.
   *
   * @return string
   *   The data name.
   */
  public function getName(): string;

  /**
   * Returns the data type.
   *
   * @return string
   *   The data type.
   */
  public function getType(): string;

  /**
   * Returns the data value formatted as string.
   *
   * @return string
   *   The data value.
   */
  public function getValue(): string;

  /**
   * Returns the child data objects.
   *
   * @return \Drupal\field_compare\FieldConfigDataInterface[]
   *   The children.
   */
  public function getChildren(): array;

  /**
   * Returns the data hash.
   *
   * @return string
   *   The data hash.
   */
  public function getHash(): string;

}
