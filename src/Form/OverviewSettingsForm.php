<?php

namespace Drupal\field_compare\Form;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\field_compare\OverviewSettings;
use Drupal\field_compare\OverviewSettingsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Field Compare overview settings form.
 */
class OverviewSettingsForm extends FormBase {

  /**
   * The entity display repository service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * The field compare overview settings manager.
   *
   * @var \Drupal\field_compare\OverviewSettings
   */
  protected OverviewSettings $overviewSettingsManager;

  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected CurrentRouteMatch $currentRouteMatch;

  /**
   * Constructs a form for field compare overview.
   *
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository
   *   The entity display repository service.
   * @param \Drupal\field_compare\OverviewSettingsInterface $overviewSettingsManager
   *   The field compare overview settings manager.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   The current route match service.
   */
  public function __construct(EntityDisplayRepositoryInterface $entityDisplayRepository, OverviewSettingsInterface $overviewSettingsManager, CurrentRouteMatch $currentRouteMatch) {
    $this->entityDisplayRepository = $entityDisplayRepository;
    $this->overviewSettingsManager = $overviewSettingsManager;
    $this->currentRouteMatch = $currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_display.repository'),
      $container->get('field_compare.settings'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_compare_overview_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $settings = $this->overviewSettingsManager->getSettings($this->getCurrentEntityType());

    $form['visible'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Configuration groups'),
      '#default_value' => $settings['visible'],
      '#options' => [
        'field' => $this->t('Field settings'),
        'storage' => $this->t('Storage settings'),
        'widget' => $this->t('Widget settings'),
        'formatter' => $this->t('Formatter settings'),
      ],
      '#required' => TRUE,
    ];

    $form['hide_equal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only show deviating properties. Hide properties that are equal on all field instances'),
      '#default_value' => $settings['hide_equal'],
    ];

    $form['form_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Form mode'),
      '#default_value' => $settings['form_mode'],
      '#options' => $this->getFormModes(),
      '#states' => [
        'visible' => [
          'input[name="visible[widget]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#default_value' => $settings['view_mode'],
      '#options' => $this->getViewModes(),
      '#states' => [
        'visible' => [
          'input[name="visible[formatter]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
    ];

    $form['#attached']['drupalSettings']['FieldCompare']['settings'] = $settings;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $visible = $form_state->getValue('visible');
    $settings = [
      'visible' => array_filter($visible),
      'hide_equal' => $form_state->getValue('hide_equal'),
    ];
    if ($visible['widget']) {
      $settings['form_mode'] = $form_state->getValue('form_mode');
    }
    if ($visible['formatter']) {
      $settings['view_mode'] = $form_state->getValue('view_mode');
    }

    $this->overviewSettingsManager->setSettings($this->getCurrentEntityType(), $settings);
    $routeName = $this->currentRouteMatch->getRouteName();
    $params = $this->currentRouteMatch->getRawParameters()->all();
    $form_state->setRedirect($routeName, $params, ['query' => $settings]);
  }

  /**
   * Returns the entity type the Compare field configuration overview uses.
   *
   * @return string
   *   The machine name of the entity type.
   */
  protected function getCurrentEntityType(): string {

    $entityType = $this->getRouteMatch()->getParameter('entity_type');
    return $entityType ?: 'node';
  }

  /**
   * Returns the available form modes of the current entity type.
   *
   * @return array
   *   Associative array of entity form modes labels. Keyed by their machine
   *   names.
   */
  protected function getFormModes(): array {
    $modes = ['default' => $this->t('Default')];
    foreach ($this->entityDisplayRepository->getFormModes($this->getCurrentEntityType()) as $key => $mode) {
      $modes[$key] = $mode['label'];
    }

    return $modes;
  }

  /**
   * Returns the available view modes of the current entity type.
   *
   * @return array
   *   Associative array of entity view modes labels. Keyed by their machine
   *   names.
   */
  protected function getViewModes(): array {
    $modes = ['default' => $this->t('Default')];
    foreach ($this->entityDisplayRepository->getViewModes($this->getCurrentEntityType()) as $key => $mode) {
      $modes[$key] = $mode['label'];
    }

    return $modes;
  }

}
