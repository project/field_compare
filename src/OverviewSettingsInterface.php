<?php

namespace Drupal\field_compare;

/**
 * The OverviewSettings service.
 */
interface OverviewSettingsInterface {

  /**
   * Returns the overview settings from storage and (optional) from query keys.
   *
   * The settings are stored per user. Values may be overridden by values from
   * URL query keys.
   *
   * @param string $entityType
   *   The entity type for which to get the settings.
   * @param bool $override
   *   Override the overview settings using values from the URL query keys. When
   *   not overridden the values will be taken from the private store.
   *
   * @return array
   *   Associative array of overview settings. Keys by settings machine name.
   */
  public function getSettings(string $entityType, bool $override = TRUE): array;

  /**
   * Update stored setting.
   *
   * @param string $entityType
   *   The entity type where the settings apply to.
   * @param array $settings
   *   Associative array of overview settings. Keys by settings machine name.
   *   Only valid keys are accepted.
   */
  public function setSettings(string $entityType, array $settings);

}
