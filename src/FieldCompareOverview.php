<?php

namespace Drupal\field_compare;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Schema\Ignore;
use Drupal\Core\Config\Schema\Mapping;
use Drupal\Core\Config\Schema\TypedConfigInterface;
use Drupal\Core\Config\Schema\Undefined;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\PrimitiveInterface;
use Drupal\Core\TypedData\TraversableTypedDataInterface;
use Drupal\Core\TypedData\Type\BooleanInterface;
use Drupal\Core\TypedData\Type\FloatInterface;
use Drupal\Core\TypedData\Type\IntegerInterface;
use Drupal\Core\TypedData\Type\StringInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\Core\Url;

/**
 * A service for the Field Compare configuration overview.
 */
class FieldCompareOverview implements FieldCompareOverviewInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The typed config manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected TypedConfigManagerInterface $typedConfigManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The entity type to use.
   *
   * @var string
   */
  protected string $entityType;

  /**
   * Static cache of field configuration per bundle.
   *
   * @var \Drupal\field\FieldConfigInterface[]
   */
  protected array $entityTypeFieldConfig;

  /**
   * Constructs a FieldCompareOverview object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger channel factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, ConfigFactoryInterface $configFactory, TypedConfigManagerInterface $typedConfigManager, ModuleHandlerInterface $moduleHandler, MessengerInterface $messenger, LoggerChannelFactoryInterface $loggerFactory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->configFactory = $configFactory;
    $this->typedConfigManager = $typedConfigManager;
    $this->moduleHandler = $moduleHandler;
    $this->messenger = $messenger;
    $this->logger = $loggerFactory->get('field_compare');
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntityTypeTable(string $entityType, string $formMode = 'default', string $viewMode = 'default'): array {
    $this->setEntityType($entityType);

    $header = [
      '_field' => [
        'data' => $this->t('Field'),
        'class' => 'col-field-name',
      ],
    ];
    $rows = [];
    $bundles = $this->getAllBundles();
    foreach ($bundles as $bundleName => $label) {
      $header[$bundleName] = [
        'data' => $label,
        'class' => 'col-entity-config',
      ];
    }

    foreach ($this->getAllFields() as $fieldName) {
      $row = [
        '_field' => $fieldName,
      ];
      foreach (array_keys($bundles) as $bundleName) {
        $row[$bundleName] = [
          'data' => $this->renderFieldConfig($bundleName, $fieldName, $formMode, $viewMode),
          'class' => 'col-entity-config',
        ];
      }
      $rows[] = [
        'data' => $row,
        'class' => 'js-field-compare-field',
        'data-field-compare-field' => $fieldName,
      ];
    }

    return [
      '#type' => 'table',
      '#header' => $header,
      '#sticky' => TRUE,
      '#rows' => $rows,
      '#empty' => $this->t('This entity type has no fields configured.'),
      '#cache' => ['max-age' => 0],
      '#attributes' => ['class' => ['field-compare-overview']],
      '#attached' => ['library' => ['field_compare/admin']],
    ];
  }

  /**
   * Builds a render array of field configuration.
   *
   * @param string $bundle
   *   The bundle on which the field is used.
   * @param string $fieldName
   *   The field machine name.
   * @param string $formMode
   *   The form mode to use.
   * @param string $viewMode
   *   The view mode to use.
   *
   * @return array
   *   The Drupal render array. If the field is not configured on this bundle,
   *   the render array is empty.
   */
  protected function renderFieldConfig(string $bundle, string $fieldName, string $formMode = 'default', string $viewMode = 'default'): array {
    if (!$this->bundleHasField($bundle, $fieldName)) {
      return [];
    }

    $data = [];
    foreach ($this->getFieldConfigProperties() as $property) {
      $data += $this->getFieldConfigData($bundle, $fieldName, $property);
    }
    $build[] = [
      '#theme' => 'field_compare_group',
      '#label' => $this->t('Field settings'),
      '#field' => $fieldName,
      '#group' => self::FIELD_SETTINGS_GROUP_NAME,
      '#data' => $data,
    ];

    $data = [];
    foreach ($this->getStorageConfigProperties() as $property) {
      $data += $this->getFieldStorageConfigData($fieldName, $property);
    }

    $build[] = [
      '#theme' => 'field_compare_group',
      '#label' => $this->t('Storage settings'),
      '#field' => $fieldName,
      '#group' => self::STORAGE_SETTINGS_GROUP_NAME,
      '#data' => $data,
    ];

    $data = $this->getFieldWidgetConfigData($bundle, $formMode, $fieldName);
    $build[] = [
      '#theme' => 'field_compare_group',
      '#label' => $this->t('Widget settings'),
      '#field' => $fieldName,
      '#group' => self::WIDGET_SETTINGS_GROUP_NAME,
      '#data' => $data,
    ];

    $data = $this->getFieldFormatterConfigData($bundle, $viewMode, $fieldName);
    $build[] = [
      '#theme' => 'field_compare_group',
      '#label' => $this->t('Formatter settings'),
      '#field' => $fieldName,
      '#group' => self::FORMATTER_SETTINGS_GROUP_NAME,
      '#data' => $data,
    ];

    return $build;
  }

  /**
   * Determine if a bundle has a particular field.
   *
   * @param string $bundle
   *   The bundle id.
   * @param string $fieldName
   *   The field machine name.
   *
   * @return bool
   *   True if the field is available in this bundle.
   */
  protected function bundleHasField(string $bundle, string $fieldName): bool {
    $key = "{$this->entityType}.{$bundle}.{$fieldName}";
    return isset($this->entityTypeFieldConfig[$bundle][$key]);
  }

  /**
   * Returns all bundles of the current entity type.
   *
   * @return array
   *   Associative array of bundle names. Keyed by bundle id, sorted descending
   *   by label.
   */
  protected function getAllBundles(): array {

    $bundleInfo = $this->entityTypeBundleInfo->getBundleInfo($this->entityType);
    uasort($bundleInfo, [self::class, 'sortByLabelProperty']);

    $bundles = [];
    foreach ($bundleInfo as $id => $info) {
      $bundles[$id] = $info['label'];
    }

    return $bundles;
  }

  /**
   * Sort by label property helper.
   *
   * @param array $a
   *   First item for comparison.
   * @param array $b
   *   Second item for comparison.
   *
   * @return int
   *   The comparison result for uasort().
   */
  protected static function sortByLabelProperty(array $a, array $b): int {
    return SortArray::sortByKeyString($a, $b, 'label');
  }

  /**
   * Returns all configurable fields of the current entity type.
   *
   * @return array
   *   Array of field machine name.
   */
  protected function getAllFields(): array {

    $storageDefinitions = $this->entityFieldManager->getFieldStorageDefinitions($this->entityType);
    $fields = [];
    foreach ($storageDefinitions as $fieldName => $definition) {
      if (!$definition->isBaseField()) {
        $fields[] = $fieldName;
      }
    }

    return $fields;
  }

  /**
   * Returns data of given field and property.
   *
   * @param string $bundle
   *   The bundle for which the field is defined.
   * @param string $fieldName
   *   The field name for which to get the property data.
   * @param string $property
   *   The property for which to get the data.
   *
   * @return array
   *   Associative array of FieldConfigData objects. Keyed by their
   *   configuration name.
   */
  protected function getFieldConfigData(string $bundle, string $fieldName, string $property): array {
    $key = "field.field.{$this->entityType}.{$bundle}.{$fieldName}";
    return $this->getConfigData($key, $property);
  }

  /**
   * Returns data of given field storage property.
   *
   * @param string $fieldName
   *   The field name for which to get the property data.
   * @param string $property
   *   The property for which to get the data.
   *
   * @return FieldConfigDataInterface[]
   *   Associative array of FieldConfigData objects. Keyed by their
   *   configuration name.
   */
  protected function getFieldStorageConfigData(string $fieldName, string $property): array {
    $key = "field.storage.{$this->entityType}.{$fieldName}";
    return $this->getConfigData($key, $property);
  }

  /**
   * Returns data of a given field formatter property.
   *
   * @param string $bundle
   *   The entity bundle.
   * @param string $viewMode
   *   The view mode machine name.
   * @param string $fieldName
   *   The field machine name.
   *
   * @return FieldConfigDataInterface[]
   *   Associative array of FieldConfigData objects. Keyed by their
   *   configuration name.
   */
  protected function getFieldFormatterConfigData(string $bundle, string $viewMode, string $fieldName): array {
    $key = "core.entity_view_display.{$this->entityType}.{$bundle}.{$viewMode}";

    if ($this->fieldIsHidden($key, $fieldName)) {
      return [];
    }

    $data = [];
    foreach ($this->getFormatterConfigProperties() as $property) {
      $data += $this->getConfigData($key, "content.{$fieldName}.{$property}");
    }

    return $data;
  }

  /**
   * Returns data of a given field widget property.
   *
   * @param string $bundle
   *   The entity bundle.
   * @param string $formMode
   *   The form mode machine name.
   * @param string $fieldName
   *   The field machine name.
   *
   * @return FieldConfigDataInterface[]
   *   Associative array of FieldConfigData objects. Keyed by their
   *   configuration name.
   */
  protected function getFieldWidgetConfigData(string $bundle, string $formMode, string $fieldName): array {
    $key = "core.entity_form_display.{$this->entityType}.{$bundle}.{$formMode}";

    if ($this->fieldIsHidden($key, $fieldName)) {
      return [];
    }

    $data = [];
    foreach ($this->getWidgetConfigProperties() as $property) {
      $data += $this->getConfigData($key, "content.{$fieldName}.{$property}");
    }
    return $data;
  }

  /**
   * Checks if a field widget or formatter is hidden.
   *
   * @param string $name
   *   The name of the configuration.
   * @param string $fieldName
   *   The field to check.
   *
   * @return bool
   *   True if the field is hidden.
   */
  protected function fieldIsHidden(string $name, string $fieldName): bool {
    $configData = $this->configFactory->get($name)->get();
    $hiddenFields = array_filter($this->getConfigPropertyValue($configData, 'hidden'));
    return isset($hiddenFields[$fieldName]);
  }

  /**
   * Returns data of the given property.
   *
   * If the property has children, they will be included.
   *
   * @param string $configName
   *   The name of the configuration.
   * @param string $name
   *   The property name or property path. The property path of a child property
   *   is a dot-separated string of parent property name(s) and property name.
   *
   * @return FieldConfigDataInterface[]
   *   Associative array of FieldConfigData objects keyed by their property
   *   path.
   */
  protected function getConfigData(string $configName, string $name): array {

    /** @var \Drupal\field\Entity\FieldConfig $fieldConfig */
    $configData = $this->configFactory->get($configName)->get();

    try {
      $value = $this->getConfigPropertyValue($configData, $name);
    }
    catch (\InvalidArgumentException $exception) {
      return [
        $name => $this->createConfigData()
          ->setLabel($name)
          ->setName($name)
          ->setType('missing schema'),
      ];
    }

    if (!isset($configData['langcode'])) {
      $configData['langcode'] = 'en';
    }
    if (!$this->typedConfigManager->hasConfigSchema($configName)) {
      return [
        $name => $this->createConfigData()
          ->setLabel($name)
          ->setName($name)
          ->setType('missing schema'),
      ];
    }

    $schema = $this->typedConfigManager->createFromNameAndData($configName, $configData);

    return $this->getConfigPropertyData($schema, $name, $value);
  }

  /**
   * Returns the value of a (nested) property.
   *
   * If the property has children, they will be included.
   *
   * @param array $configData
   *   Configuration data.
   * @param string $name
   *   The property name or property path. The property path of a child property
   *   is a dot-separated string of parent property name(s) and property name.
   *
   * @return mixed
   *   The property value. Child property values are nested in the parent.
   *
   * @throws \InvalidArgumentException
   *   If none existing property data is requested.
   */
  protected function getConfigPropertyValue(array $configData, string $name) {

    // Process config name without children.
    if (!str_contains($name, '.')) {
      if (!isset($configData[$name])) {
        // This should not actually happen. Wait for an issue to be reported.
        throw new \InvalidArgumentException(sprintf("No configuration value can be found for property %s", $name));
      }

      return $configData[$name];
    }

    // Process config name with children.
    $parts = explode('.', $name);
    $root_key = array_shift($parts);
    if (!isset($configData[$root_key])) {
      // This should not actually happen. Wait for an issue to be reported.
      throw new \InvalidArgumentException(sprintf("No configuration value can be found for property path %s", $name));
    }

    $element = $configData[$root_key];
    while ($element && ($key = array_shift($parts)) !== NULL) {
      if ($element instanceof TypedConfigInterface) {
        $element = $element->get($key);
      }
      elseif (is_array($element) && isset($element[$key])) {
        $element = $element[$key];
      }
      else {
        $element = NULL;
      }
    }

    return $element;
  }

  /**
   * Returns the data of a given property.
   *
   * @param \Drupal\Core\TypedData\TypedDataInterface $schema
   *   The schema where the property is part of.
   * @param string $name
   *   The property name or property path. The property path of a child property
   *   is a dot-separated string of parent property name(s) and property name.
   * @param mixed $value
   *   The property value.
   *
   * @return FieldConfigDataInterface[]
   *   Associative array of FieldConfigData objects keyed by their property
   *   path.
   *
   * @throws \InvalidArgumentException
   *   In some theoretical cases when no property data can be retrieved.
   */
  protected function getConfigPropertyData(TypedDataInterface $schema, string $name, $value): array {
    /** @var \Drupal\Core\TypedData\TypedDataInterface $element */
    $element = $schema->get($name);
    if (empty($element)) {
      // Not sure if this actually can happen. Wait for an issue to be reported.
      throw new \InvalidArgumentException(sprintf('Trying to load non existing property from property path %s', $name));
    }

    if ($element instanceof Undefined) {
      $this->messenger->addWarning($this->t('Missing schema detected. Some properties may not be displayed optimally. See <a href=":url">Recent log messages</a> for more information.', [':url' => Url::fromRoute('dblog.overview')->toString()]));

      // Find the top level parent configuration. This is the configuration of
      // the field inside the entity.
      $parent = $element->getParent();
      do {
        $topParent = $parent;
      } while ($parent = $parent->getParent());

      $messageProperties = [
        '%property_name' => $element->getName(),
        '%field_type' => $topParent->getValue()['type'] ?? $topParent->getValue()['field_type'] ?? '-- unknown field type --',
        '@property_name' => $element->getName(),
        '@$property_path' => $element->getPropertyPath(),
        '@entity_type' => $topParent->getValue()['entity_type'] ?? '-- unknown entity type --',
        '@field_name' => $topParent->getValue()['field_name'] ?? '-- unknown field name --',
      ];
      $messages[] = $this->t('Missing schema of property %property_name. You are encouraged to report a missing schema in the issue queue of the module that is providing the field type %field_type.', $messageProperties);
      $messages[] = $this->t('Property: @property_name (part of @$property_path)', $messageProperties);
      $messages[] = $this->t('Entity: @entity_type, field: @field_name', $messageProperties);
      $this->logger->warning(implode('<br />', $messages));
      return [
        $name => $this->createConfigData()
          ->setLabel($element->getName())
          ->setName($name)
          ->setType('missing schema')
          ->setValue($this->valueToString('missing schema', $value)),

      ];
    }

    // Do not check value if it is defined to be ignored.
    if ($element instanceof Ignore) {
      return [];
    }

    $label = $element->getDataDefinition()->getLabel();
    if ($element instanceof PrimitiveInterface) {
      if ($element instanceof StringInterface) {
        $type = 'string';
      }
      elseif ($element instanceof BooleanInterface) {
        $type = 'boolean';
      }
      elseif ($element instanceof IntegerInterface) {
        $type = 'integer';
      }
      elseif ($element instanceof FloatInterface) {
        $type = 'float';
      }
      else {
        $type = 'string';
      }

      return [
        $name => $this->createConfigData()
          ->setLabel($label)
          ->setName($name)
          ->setType($type)
          ->setValue($this->valueToString($type, $value)),
      ];
    }

    if ($element instanceof TraversableTypedDataInterface) {
      $type = $element instanceof Mapping ? 'mapping' : 'sequence';
      if (!is_array($value)) {
        $value = (array) $value;
      }
      $children = [];
      foreach ($value as $nested_value_key => $nested_value) {
        $children += $this->getConfigPropertyData($schema, $name . '.' . $nested_value_key, $nested_value);
      }
      return [
        $name => $this->createConfigData()
          ->setLabel($element->getDataDefinition()->getLabel())
          ->setName($name)
          ->setType($type)
          ->setChildren(array_filter($children)),
      ];
    }

    // Not sure if this actually can happen. Wait for an issue to be reported.
    throw new \InvalidArgumentException(sprintf('Unknown typed data element %s found when loading property from path %s', get_class($element), $name));
  }

  /**
   * Converts a property value to string.
   *
   * @param string $type
   *   The config type.
   * @param mixed $value
   *   The raw configuration value.
   *
   * @return string
   *   The value as string.
   */
  protected function valueToString(string $type, $value): string {

    if (is_null($value)) {
      return '';
    }

    switch ($type) {
      case 'boolean':
        return $value ? '1' : '0';

      case 'integer':
      case 'float':
        return (string) $value;

      case 'string':
      case 'label':
      case 'path':
      case 'text':
      case 'date_format':
      case 'color_hex':
      case 'uri':
      case 'email':
        return $value;

      case 'sequence':
      case 'mapping':
        // These types should not have a value. Ignore it.
        return '';

      case 'missing schema':
      default:
        if (is_array($value)) {
          return print_r($value, TRUE);
        }
        else {
          return (string) $value;
        }
    }
  }

  /**
   * The selection of field config properties to be processed.
   *
   * @return string[]
   *   Array of property machine names.
   */
  protected function getFieldConfigProperties(): array {

    $properties = [
      'label',
      'required',
      'default_value',
      'description',
      'settings',
    ];

    $groupName = self::FIELD_SETTINGS_GROUP_NAME;
    $this->moduleHandler->alter('field_compare_field_properties', $properties, $groupName);

    return $properties;
  }

  /**
   * The field storage config properties that will be displayed.
   *
   * This is a selection of storage properties that are useful to be displayed.
   *
   * @return string[]
   *   Array of property machine names.
   */
  protected function getStorageConfigProperties(): array {

    $properties = [
      0 => 'cardinality',
      2 => 'settings',
    ];
    if ($this->moduleHandler->moduleExists('content_translation')) {
      $properties[1] = 'translatable';
    }

    $groupName = self::STORAGE_SETTINGS_GROUP_NAME;
    $this->moduleHandler->alter('field_compare_field_properties', $properties, $groupName);

    return $properties;
  }

  /**
   * The field widget config properties that will be displayed.
   *
   * This is a selection of properties that are useful to be displayed.
   *
   * @return string[]
   *   Array of property machine names.
   */
  protected function getWidgetConfigProperties(): array {

    $properties = [
      'settings',
      'third_party_settings',
    ];

    $groupName = self::WIDGET_SETTINGS_GROUP_NAME;
    $this->moduleHandler->alter('field_compare_field_properties', $properties, $groupName);

    return $properties;
  }

  /**
   * The field formatter config properties that will be displayed.
   *
   * This is a selection of properties that are useful to be displayed.
   *
   * @return string[]
   *   Array of property machine names.
   */
  protected function getFormatterConfigProperties(): array {

    $properties = [
      'settings',
      'third_party_settings',
    ];

    $groupName = self::FORMATTER_SETTINGS_GROUP_NAME;
    $this->moduleHandler->alter('field_compare_field_properties', $properties, $groupName);

    return $properties;
  }

  /**
   * Set the entity type this service uses.
   *
   * @param string $entityType
   *   The entity type machine name.
   */
  protected function setEntityType(string $entityType) {
    $this->entityType = $entityType;
    foreach (array_keys($this->getAllBundles()) as $bundle) {
      $this->entityTypeFieldConfig[$bundle] = $this->entityTypeManager->getStorage('field_config')
        ->loadByProperties([
          'entity_type' => $this->entityType,
          'bundle' => $bundle,
        ]);
    }
  }

  /**
   * Creates a field config data object.
   *
   * @return \Drupal\field_compare\FieldConfigData
   *   An empty object.
   */
  protected function createConfigData(): FieldConfigDataInterface {
    return new FieldConfigData();
  }

}
