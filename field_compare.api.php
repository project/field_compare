<?php

/**
 * @file
 * Field Compare API documentation.
 */

/**
 * @addtogroup hooks
 * @{
 */

use Drupal\field_compare\FieldCompareOverviewInterface;

/**
 * Perform alterations on the properties to be displayed by Field Compare.
 *
 * @param array $properties
 *   The properties to be altered. Child properties can used by providing a
 *   property path, a dot separated string of parent and child property names.
 * @param string $group_name
 *   The group name of the provided properties. Four groups are available:
 *   - field: Field properties as found in
 *     field.field.{entity_type}.{bundle}.{field_name} files.
 *   - storage: Field storage properties as found in
 *     field.storage.{entity_type}.{field_name} files.
 *   - widget: Field widget properties as found in
 *     core.entity_view_display.{entity_type}.{bundle}.{view_mode} files.
 *   - formatter: Field formatter properties as found in
 *     core.entity_form_display.{entity_type}.{bundle}.{form_mode} files.
 *
 * @see \Drupal\field_compare\FieldCompareOverview::getFieldConfigProperties
 * @see \Drupal\field_compare\FieldCompareOverview::getStorageConfigProperties
 * @see \Drupal\field_compare\FieldCompareOverview::getWidgetConfigProperties
 * @see \Drupal\field_compare\FieldCompareOverview::getFormatterConfigProperties
 */
function hook_field_compare_field_properties_alter(array &$properties, string $group_name) {

  switch ($group_name) {
    case FieldCompareOverviewInterface::FIELD_SETTINGS_GROUP_NAME:
    case FieldCompareOverviewInterface::STORAGE_SETTINGS_GROUP_NAME:
      $properties[] = 'langcode';
      break;
  }
}

/**
 * @} End of "addtogroup hooks".
 */
