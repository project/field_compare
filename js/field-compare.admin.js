(function ($, Drupal) {

  'use strict';

  /**
   * Gives configuration groups a number to identify unique configurations.
   */
  Drupal.behaviors.fieldCompareIdentifyConfigVariations = {
    attach(context) {
      const variations = new Map;

      /**
       * Calculate the variation number.
       *
       * Variations are based on the hash and are unique per field /
       * configuration group combination
       *
       * @param {string} type
       *   The type of the configuration group.
       * @param {string} hash
       *   The group hash, identifying unique item values.
       *
       * @returns {int}
       *   The variation number, starting at 1.
       */
      const getVariationNumber = function (type, hash) {
        if (variations.get(type)) {
          const groupVariations = variations.get(type);
          if (groupVariations.get(hash)) {
            return groupVariations.get(hash);
          }
          else {
            groupVariations.set(hash, groupVariations.size + 1);
            return groupVariations.get(hash);
          }
        }
        else {
          const groupVariations = new Map;
          variations.set(type, groupVariations.set(hash, 1));
          return 1;
        }
      }

      /**
       * Clear the variation data.
       */
      const resetVariations = function () {
        variations.clear();
      }

      $('.js-field-compare-field', context).each(function () {
        $(this).find('.js-field-compare-group[data-field-compare-level="1"]').each(function () {
          const $group = $(this);
          if (!$group.hasClass('has-variation')) {
            const groupHash = $(this).data('field-compare-group-hash');
            const groupType = $(this).data('field-compare-group');
            const variation = getVariationNumber(groupType, groupHash);
            const $notification = $(`<span class="field-compare-notification field-compare-notification-inline">${variation}</span>`);
            $group.find('.field-compare-group-label')
              .wrap('<div class="notification-wrapper"></div>')
              .append($notification);
            $group.addClass('has-variation');
          }
        });
        resetVariations();
      });

    }
  };

  /**
   * Handle interaction with the overview settings form.
   */
  Drupal.behaviors.fieldCompareSettingsForm = {
    attach(context) {
      const $form = $('.field-compare-overview-settings', context);
      if (!$form.length) {
        return;
      }

      this.timeoutID = undefined;

      /**
       * Show or hide a group.
       *
       * @param groupType
       *   The group type.
       * @param status
       *   True to display the group, false to hide.
       */
      const showGroup = function (groupType, status) {
        const $groups = $(`.js-field-compare-group[data-field-compare-group="${groupType}"]`, context);
        if (status) {
          $groups.removeClass('field-compare-hidden-group').show();
        }
        else {
          $groups.addClass('field-compare-hidden-group').hide();
        }
      }

      /**
       * Hide groups that are equal in all field instances.
       *
       * A placeholder is added to the parent group if all items are
       * hidden.
       *
       * @param {boolean} hide
       *   True to hide equal items. False to show them.
       */
      const hideEqualGroups = function (hide) {
        $('.js-field-compare-field', context).each(function () {
          const $field = $(this);

          if (!hide) {
            $field.find('.js-field-compare-group').filter(':not(.field-compare-hidden-group)').show();
            return;
          }

          $field.find('.js-field-compare-group[data-field-compare-level="1"]').each(function () {
            const $group = $(this);

            // Skip this group if it is already hidden.
            if ($group.is(':hidden')) {
              return;
            }

            const groupType = $group.data('field-compare-group');
            const groupHash = $group.data('field-compare-group-hash');
            const $groupsOfSameType = $field.find(`.js-field-compare-group[data-field-compare-level="1"][data-field-compare-group="${groupType}"]`);
            const $groupsWithSameHash = $groupsOfSameType.filter(`[data-field-compare-group-hash="${groupHash}"]`);

            // If there is only one groups with this type, hide it.
            if ($groupsOfSameType.length === 1) {
              $group.hide();
              return;
            }

            // If not all groups have the same values (hash), don't hide them.
            if ($groupsOfSameType.length > $groupsWithSameHash.length) {
              return;
            }

            $groupsOfSameType.hide();
          });
        });
      }

      /**
       * Hide items that are equal in all field instances.
       *
       * @param {boolean} hide
       *   True to hide equal items, false to show them.
       */
      const hideEqualItems = function (hide) {
        $('.js-field-compare-field', context).each(function () {
          const $field = $(this);

          if (!hide) {
            $field.find('.js-field-compare-items').filter(':not(.field-compare-hidden-group)').show();
            $field.find('.js-field-compare-item').filter(':not(.field-compare-hidden-group)').show();
            return;
          }

          $field.find('.js-field-compare-group[data-field-compare-level="1"]').each(function () {
            const $group = $(this);

            // Skip this group if all items are already hidden.
            if ($group.find('> .js-field-compare-items').is(':hidden')) {
              return;
            }

            // Find ANY child item (also at deeper levels) and hide if needed.
            $group.find('.js-field-compare-item').each(function () {
              const $item = $(this);

              // Skip if the immediate parent group is hidden.
              if ($item.closest('.js-field-compare-items').is(':hidden')) {
                return;
              }

              // Skip if this item is already hidden.
              if ($item.is(':hidden')) {
                return;
              }

              comparableItems($item).hide();

              // When the last item of a group gets hidden, we hide the group it
              // belongs too. Next, traverse upwards because the group may have
              // been the last item from its parent group too. Continue
              // recursively.
              let $parentGroup;
              let $traversalItem = $item;
              if ($traversalItem.is(':hidden')) {
                while ($traversalItem) {
                  if ($traversalItem.siblings(':visible').length === 0) {
                    $parentGroup = $item.closest('.js-field-compare-group');
                    if ($parentGroup.is(':visible')) {
                      $parentGroup.hide();
                      // Stop before the top level group, which is covered by
                      // hideEqualGroups().
                      if ($parentGroup.data('field-compare-level') > 1) {
                        $traversalItem = $parentGroup.closest('.js-field-compare-item');
                        comparableItems($traversalItem).hide();
                      } else {
                        $traversalItem = false;
                      }
                    } else {
                      $traversalItem = false;
                    }
                  } else {
                    $traversalItem = false;
                  }
                }
              }
            });
          });
        });
      }

      /**
       * Return all items that are comparable with a given.
       *
       * Comparable items are implementation of the same field that have the
       * same name (property path) and the same value (including the value of
       * their children).
       *
       * @param $item
       * @returns {jQuery|HTMLElement}
       */
      const comparableItems = function ($item) {
        const itemId = $item.data('field-compare-item-id');
        const itemHash = $item.data('field-compare-item-hash');
        const $itemsWithSameId = $item.closest('.js-field-compare-field')
          .find(`.js-field-compare-item[data-field-compare-item-id="${itemId}"]`);
        const $itemsWithSameHash = $itemsWithSameId.filter(`[data-field-compare-item-hash="${itemHash}"]`);


        if ($itemsWithSameId.length > $itemsWithSameHash.length) {
          return $();
        }

        return $itemsWithSameId;
      }

      /**
       * Make height of adjacent groups (same field, same group type) equal.
       */
      const equalHeightGroups = function () {
        $('.js-field-compare-field', context).each(function () {
          const maxHeights = new Map();
          const $field = $(this);

          // Measure the height of each groups and store the maximum value.
          $field.find('.js-field-compare-group[data-field-compare-level="1"]').each(function () {
            const $group = $(this);
            if ($group.is(':hidden')) {
              return;
            }

            $group.height('auto');
            const groupType = $group.data('field-compare-group');
            const height = $(this).height();
            if (maxHeights.get(groupType)) {
              maxHeights.set(groupType, Math.max(height, maxHeights.get(groupType)));
            }
            else {
              maxHeights.set(groupType, height);
            }
          });

          // If all groups of a field are hidden, we can skip further
          // processing.
          if (maxHeights.size === 0) {
            return;
          }

          // Apply the maximum group height (per group type) to each group.
          $field.find('.js-field-compare-group[data-field-compare-level="1"]').each(function () {
            const $group = $(this);
            if ($group.is(':hidden')) {
              return;
            }

            const groupType = $group.data('field-compare-group');
            $(this).height(maxHeights.get(groupType));
          });

          // We are done with this field. Clear the heights for the next round.
          maxHeights.clear();
        });
      }

      /**
       * Save configuration settings in the backend and update URL accordingly.
       *
       * Uses AJAX to update the settings in the backend.
       *
       * @param $form
       *   The form element that contains the settings.
       */
      const saveSettings = function ($form) {
        // Take the entity type from the page URL for example
        // /admin/reports/field-compare/node
        const pathName = location.pathname;
        const entityType = pathName.substr(pathName.lastIndexOf('/') + 1);

        const visibleField = $form.find('input[name="visible[field]"]').is(':checked');
        const visibleStorage = $form.find('input[name="visible[storage]"]').is(':checked');
        const visibleWidget = $form.find('input[name="visible[widget]"]').is(':checked');
        const visibleFormatter = $form.find('input[name="visible[formatter]"]').is(':checked');
        const hideEqual = $form.find('input[name="hide_equal"]').is(':checked');
        const formMode = $form.find('select[name="form_mode"]').val();
        const viewMode = $form.find('select[name="view_mode"]').val();

        $.ajax({
          url: Drupal.url('admin/reports/field-compare/ajax-settings-update'),
          type: 'POST',
          data: {
            entityType: entityType,
            visibleFieldSettings: visibleField ? 'field' : '',
            visibleStorageSettings: visibleStorage ? 'storage' : '',
            visibleWidgetSettings: visibleWidget ? 'widget' : '',
            visibleFormatterSettings: visibleFormatter ? 'formatter' : '',
            hideEqual: hideEqual,
            formMode: formMode,
            viewMode: viewMode,
          },
          dataType: 'json',
        });

        const queryParams = {};
        if (visibleField) {
          queryParams['visible[field]'] = 'field';
        }
        if (visibleStorage) {
          queryParams['visible[storage]'] = 'storage';
        }
        if (visibleWidget) {
          queryParams['visible[widget]'] = 'widget';
          queryParams['form_mode'] = formMode;
        }
        if (visibleFormatter) {
          queryParams['visible[formatter]'] = 'formatter';
          queryParams['view_mode'] = viewMode;
        }
        queryParams['hide_equal'] = hideEqual ? '1' : '0';

        const params = new URLSearchParams(Object.entries(queryParams));
        const newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + params.toString();
        window.history.pushState({path:newurl},'',newurl);
      }

      once('fieldCompareSettingsForm', '.field-compare-overview-settings', context).forEach(function () {
        const $inputFieldSettings = $form.find('input[name="visible[field]"]');
        const $inputStorageSettings = $form.find('input[name="visible[storage]"]');
        const $inputWidgetSettings = $form.find('input[name="visible[widget]"]');
        const $inputFormatterSettings = $form.find('input[name="visible[formatter]"]');
        const $inputHideEqual = $form.find('input[name="hide_equal"]');
        const $inputFormMode = $form.find('select[name="form_mode"]');
        const $inputViewMode = $form.find('select[name="view_mode"]');
        const $submitButton = $form.find('.js-form-submit');

        // When settings change.
        $inputFieldSettings.on('change', function () {
          showGroup('field', $(this).is(':checked'));
          saveSettings($form);
        });
        $inputStorageSettings.on('change', function () {
          showGroup('storage', $(this).is(':checked'));
          saveSettings($form);
        });
        $inputWidgetSettings.on('change', function () {
          showGroup('widget', $(this).is(':checked'));
          saveSettings($form);
        });
        $inputFormatterSettings.on('change', function () {
          showGroup('formatter', $(this).is(':checked'));
          saveSettings($form);
        });
        $inputHideEqual.on('change', function () {
          hideEqualGroups($(this).is(':checked'));
          hideEqualItems($(this).is(':checked'));
          equalHeightGroups();
          saveSettings($form);
        });
        $inputFormMode.on('change', function () {
          $submitButton.click();
        });
        $inputViewMode.on('change', function () {
          $submitButton.click();
        });

        // Initial state.
        showGroup('field', $inputFieldSettings.is(':checked'));
        showGroup('storage', $inputStorageSettings.is(':checked'));
        showGroup('widget', $inputWidgetSettings.is(':checked'));
        showGroup('formatter', $inputFormatterSettings.is(':checked'));
        hideEqualGroups($inputHideEqual.is(':checked'));
        hideEqualItems($inputHideEqual.is(':checked'));
        equalHeightGroups();

        $submitButton.hide();

        // Make heights equal on window resize.
        window.addEventListener('resize', function (event) {
          if (typeof this.timeoutID === 'number') {
            clearTimeout(this.timeoutID);
          }

          this.timeoutID = setTimeout(equalHeightGroups, 500);
        });

      });
    }
  };

})(jQuery, Drupal);
